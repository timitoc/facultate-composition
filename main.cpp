#include <iostream>
#include <Composition.h>

int main() {

    Composition compo("(C6H12O6)3(SO4(Fl2Ne3)2)3");
    std::cout << compo.ToString();
    return 0;
}