//
// Created by timi on 25.11.2018.
//

#ifndef PPLUS_COMPOSITION_H
#define PPLUS_COMPOSITION_H

#include <string>
#include <map>

class Composition {
public:
    explicit Composition(const std::string& formula);
    Composition();
    std::string ToString() const;
    Composition&operator+=(const Composition& other);
    Composition&operator*=(uint factor);
    friend Composition atomComposition(const std::string& formula, uint& index);

private:
    Composition SumExpression(const std::string& formula, uint& index);
    Composition FactorExpression(const std::string& formula, uint& index);
    Composition AtomExpression(const std::string& formula, uint& index);

private:
    std::map<std::string, uint> m_atomCount;
};

Composition atomComposition(const std::string& formula, uint& index);

#endif //PPLUS_COMPOSITION_H
