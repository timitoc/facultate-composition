//
// Created by timi on 25.11.2018.
//

#include <numeric>
#include "Composition.h"

Composition::Composition(const std::string &formula) {
    uint index = 0;
    *this = SumExpression(formula, index);
    if (index != formula.size())
        throw std::domain_error("Early closure");
}

Composition::Composition() {

}

std::string Composition::ToString() const {
    if (m_atomCount.empty())
        return "";
    std::string res = std::accumulate(std::next(m_atomCount.begin()), m_atomCount.end(),
            m_atomCount.begin()->first + ": " + std::to_string(m_atomCount.begin()->second),
            [](std::string a, std::pair<std::string, uint> p) {
                return std::move(a) + "\n" + p.first + ": " + std::to_string(p.second);
            });
    return res;
}

Composition atomComposition(const std::string& formula, uint& index) {
    if (index >= formula.size())
        throw std::out_of_range("Index out of formula bounds");
    Composition composition;
    std::string atom;
    if (!isupper(formula[index]))
        throw std::domain_error("First character of atom must be uppercase letter");
    atom += formula[index++];
    if (index < formula.size() && islower(formula[index]))
        atom += formula[index++];
    composition.m_atomCount[atom] = 1;
    return composition;
}

Composition &Composition::operator+=(const Composition &other) {
    for (const auto&[atom, count] : other.m_atomCount)
        m_atomCount[atom] += count;
    return *this;
}

Composition &Composition::operator*=(uint factor) {
    for (const auto&[atom, count] : m_atomCount)
        m_atomCount[atom] *= factor;
    return *this;
}

Composition Composition::SumExpression(const std::string &formula, uint &index) {
    Composition composition = FactorExpression(formula, index);
    while (index < formula.size() && (isalpha(formula[index]) || formula[index] == '('))
        composition += FactorExpression(formula, index);
    if (index < formula.size() && !isalpha(formula[index]) && formula[index] != '(' && formula[index] != ')')
        throw std::domain_error("Invalid token " + formula.substr(index, 1));
    return composition;
}

Composition Composition::FactorExpression(const std::string &formula, uint &index) {
    Composition composition = AtomExpression(formula, index);
    uint factor = 0;
    while (index < formula.size() && isdigit(formula[index]))
        factor = factor*10 + formula[index++] - '0';
    if (factor) composition *= factor;
    return composition;
}

Composition Composition::AtomExpression(const std::string &formula, uint &index) {
    if (index >= formula.size())
        return Composition();
    if (formula[index] == '(') {
        Composition composition = SumExpression(formula, ++index);
        if (formula[index] != ')')
            throw std::domain_error("Unmatched parenthesis");
        index++; /// Jumps over ')'
        return composition;
    }
    return atomComposition(formula, index);
}

