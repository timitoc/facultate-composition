#include "gtest/gtest.h"

#include <Composition.h>
#include "gtest/gtest.h"

class CompositionTestFixture : public ::testing::Test {
protected:
    virtual void TearDown() {
    }

    virtual void SetUp() {
    }

public:
    CompositionTestFixture() : Test() {

    }

    virtual ~CompositionTestFixture() {

    }

};

TEST_F(CompositionTestFixture, CheckEmptyComposition) {
    Composition composition;
    EXPECT_EQ(composition.ToString(), "");
}

TEST_F(CompositionTestFixture, CheckOnelLetterAtomComposition) {
    uint index = 0;
    Composition composition = atomComposition("C", index);
    EXPECT_EQ(composition.ToString(), "C: 1");
    EXPECT_EQ(index, 1);
}

TEST_F(CompositionTestFixture, CheckTwoLetterAtomComposition) {
    uint index = 0;
    Composition composition = atomComposition("Cl", index);
    EXPECT_EQ(composition.ToString(), "Cl: 1");
    EXPECT_EQ(index, 2);
}

TEST_F(CompositionTestFixture, CheckAtomCompositionWithOffset) {
    uint index = 1;

    Composition composition = atomComposition("SHFl", index);
    EXPECT_EQ(composition.ToString(), "H: 1");
    EXPECT_EQ(index, 2);
}

TEST_F(CompositionTestFixture, CheckCompositionSelfAddNewAtom) {
    /// Arrange
    uint index = 0;
    Composition mainComposition = atomComposition("H", index);
    index = 0;
    Composition toAdd = atomComposition("O", index);
    /// Act
    mainComposition += toAdd;
    /// Assert
    EXPECT_EQ(mainComposition.ToString(), "H: 1\nO: 1");
}

TEST_F(CompositionTestFixture, CheckCompositionSelfAddSameAtom) {
    /// Arrange
    uint index = 0;
    Composition mainComposition = atomComposition("H", index);
    index = 0;
    Composition toAdd = atomComposition("H", index);
    /// Act
    mainComposition += toAdd;
    /// Assert
    EXPECT_EQ(mainComposition.ToString(), "H: 2");
}

TEST_F(CompositionTestFixture, CheckCompositionSelfScalarMultiplication) {
    /// Arrange
    uint index = 0;
    Composition mainComposition = atomComposition("H", index);
    /// Act
    mainComposition *= 2;
    /// Assert
    EXPECT_EQ(mainComposition.ToString(), "H: 2");
}

TEST_F(CompositionTestFixture, CheckSimpleSumFormula) {
    Composition composition("HO");
    EXPECT_EQ(composition.ToString(), "H: 1\nO: 1");
}

TEST_F(CompositionTestFixture, CheckSimpleMultiplicationFormula) {
    Composition composition("H2");
    EXPECT_EQ(composition.ToString(), "H: 2");
}

TEST_F(CompositionTestFixture, CheckMixedSumMultiplicationFormula) {
    Composition composition("H2O");
    EXPECT_EQ(composition.ToString(), "H: 2\nO: 1");
}

TEST_F(CompositionTestFixture, CheckParanthesisFormula) {
    Composition composition("Fe2(SO4)3");
    EXPECT_EQ(composition.ToString(), "Fe: 2\nO: 12\nS: 3");
}

TEST_F(CompositionTestFixture, CheckOutOfBoundsAtomException) {
    uint index = 2;
    EXPECT_THROW(Composition composition = atomComposition("Fe", index), std::out_of_range);
}

TEST_F(CompositionTestFixture, CheckInvalidAtomException) {
    uint index = 0;
    std::string expectedError = "First character of atom must be uppercase letter";
    try {
        Composition composition = atomComposition("fe", index);
        FAIL() << "Expected std::domain_error";
    }
    catch(std::domain_error const & err) {
        EXPECT_EQ(err.what(), expectedError);
    }
    catch(...) {
        FAIL() << "Expected std::domain_error";
    }
}

TEST_F(CompositionTestFixture, CheckUnmatchedParanthesisException) {
    std::string expectedError = "Unmatched parenthesis";
    try {
        Composition composition("H2(SO3Fe");
        FAIL() << "Expected std::domain_error";
    }
    catch(std::domain_error const & err) {
        EXPECT_EQ(err.what(), expectedError);
    }
    catch(...) {
        FAIL() << "Expected std::domain_error";
    }
}

TEST_F(CompositionTestFixture, CheckInvalidTokenException) {
    std::string expectedError = "Invalid token -";
    try {
        Composition composition("H2(SO3)-Fe");
        FAIL() << "Expected std::domain_error";
    }
    catch(std::domain_error const & err) {
        EXPECT_EQ(err.what(), expectedError);
    }
    catch(...) {
        FAIL() << "Incorrect Exception. Expected std::domain_error";
    }
}

TEST_F(CompositionTestFixture, CheckInvalidClosureException) {
    std::string expectedError = "Early closure";
    try {
        Composition composition("H2(SO3))Fe");
        FAIL() << "Expected std::domain_error";
    }
    catch(std::domain_error const & err) {
        EXPECT_EQ(err.what(), expectedError);
    }
    catch(...) {
        FAIL() << "Incorrect Exception. Expected std::domain_error";
    }
}